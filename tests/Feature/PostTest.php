<?php

namespace Tests\Feature;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{

    /**  @test */
    public function the_application_returns_a_successful_response()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**  @test */
    public function store_comment()
    {
        $this->withoutExceptionHandling();

        $res = $this->post('/api/comment/store', [
            'name' => 'ali',
            'body' => 'this is comment',
            'post_id' => 1,
        ]);

        $post = Post::find(1);

        $flag = 0;
        $post->comments->each(function ($comment, $key) use (&$flag) {
            if ($comment->name == 'ali') $flag++; 
        });


        $this->assertEquals($flag, 1);
    }

    /**  @test */
    public function fetch_comment()
    {
        $this->withoutExceptionHandling();

        $res = $this->get('/api/comment/2');

        $res->assertStatus(200);
    }
}
