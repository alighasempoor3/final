<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <title>Laravel</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    
</head>

<body>
    <div id="app">
        <router-view/>
        
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
