import { createRouter, createWebHistory } from 'vue-router'
import store from './store'

import Post from './pages/Post.vue'
import Login from './pages/auth/Login.vue'
import Register from './pages/auth/Register.vue'
import AdminRegister from './pages/admin/auth/Register.vue'
import AdminLogin from './pages/admin/auth/Login.vue'

import PostCreate from './pages/admin/post/Create.vue'
import PostList from './pages/admin/post/List.vue'
import PostEdit from './pages/admin/post/Edit.vue'
import PostDetail from './pages/PostDetail.vue'

async function checkAuth() {
    if (!store.state.auth) {
        await axios.get('/api/auth/check').then(res => {
            if (res.data) store.state.auth = 1
        })
    }
}

async function checkSuperAdminAuth() {
    if (!store.state.superAdminAuth) {
        await axios.get('/api/auth/admin/check').then(res => {
            if (res.data) store.state.superAdminAuth = 1
        })
    }
}

async function checkAdminAuth() {
    if (!store.state.adminAuth) {
        await axios.get('/api/auth/admin/check').then(res => {
            if (res.data) store.state.adminAuth = 1
        })
    }
}


const routes = [
    { path: '/post', component: Post },
    {
        path: '/login', name: 'login', component: Login,
        beforeEnter: async (to, from, next) => {
            await checkAuth()
            if ((store.state.auth)) next('/')
            else next()

        },
    },
    {
        path: '/register', name: 'register', component: Register,
        beforeEnter: async (to, from, next) => {
            await checkAuth()
            if ((store.state.auth)) next('/')
            else next()
        },
    },
    {
        path: '/admin/register', name: 'admin.register', component: AdminRegister,
        beforeEnter: async (to, from, next) => {
            await checkSuperAdminAuth()
            if ((store.state.superAdminAuth)) next('/')
            else next()
        },
    },
    {
        path: '/admin/login', name: 'admin.login', component: AdminLogin,
        beforeEnter: async (to, from, next) => {
            await checkAdminAuth()
            if ((store.state.adminAuth)) next('/')
            else next()

        },
    },
    {
        path: '/admin/post/create', component: PostCreate,
        beforeEnter: async (to, from, next) => {
            await checkAdminAuth()
            if (store.state.adminAuth) next()
            else next('/admin/login')
        },
    },
    {
        path: '/admin/post/list', component: PostList,
        beforeEnter: async (to, from, next) => {
            await checkAdminAuth()
            if (store.state.adminAuth) next()
            else next('/admin/login')
        },
    },
    {
        path: '/admin/post/edit/:id', component: PostEdit,
        beforeEnter: async (to, from, next) => {
            await checkAdminAuth()
            if (store.state.adminAuth) next()
            else next('/admin/login')
        },
    },
    ,
    {
        path: '/post/:id',
        component: PostDetail,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
