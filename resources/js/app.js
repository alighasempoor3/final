require('./bootstrap')

import {createApp} from 'vue'
import router from './routes'
import store from './store'
import LaravelVuePagination from 'laravel-vue-pagination'

const app = createApp({})

app.use(router)
app.use(store)

app.component('Pagination', LaravelVuePagination)

app.mount('#app')