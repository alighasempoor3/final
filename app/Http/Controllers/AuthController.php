<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Validation\Rules\Password;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => ['required', 'confirmed', Password::min(4)],
        ]);

        $user = User::create([
            'name' => $req['name'],
            'email' => $req['email'],
            'password' => Hash::make($req['password'])
        ]);

        Auth::login($user);
    }

    public function login(Request $req)
    {
        $credentials = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $req->session()->regenerate();

            return response()->json(Auth::user(), 200);
        }
        return response()->json('unauthorized', 401);
    }

    public function usersFetch()
    {
        $users = User::all();

        return response()->json($users);
    }

    public function authCheck()
    {
        if (Auth::check()) {
            return response()->json(1, 200);
        }
        return response()->json(0, 200);
    }

    public function superAdminAuthCheck()
    {
        if (Auth::guard('admin')->check()) {
            return response()->json(1, 200);
        }
        return response()->json(0, 200);
    }

    public function adminRegister(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'password' => ['required', 'confirmed', Password::min(4)],

        ]);

        $admin = Admin::create([
            'name' => $req['name'],
            'email' => $req['email'],
            'password' => Hash::make($req['password'])
        ]);

        Auth::guard('admin')->login($admin);
    }

    public function adminLogin(Request $req)
    {
        $credentials = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('admin')->attempt($credentials)) {
            $req->session()->regenerate();

            return response()->json(Auth::user(), 200);
        }
        return response()->json('unauthorized', 401);
    }

    public function adminAuthCheck()
    {
        if (Auth::guard('admin')->check()) {
            return response()->json(1, 200);
        }
        return response()->json(0, 200);
    }
}
