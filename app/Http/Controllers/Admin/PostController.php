<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    public function __construct()
    {
        config(['auth.defaults.guard' => 'admin']);
    }

    public function store(Request $req)
    {
        $req->validate([
            'title' => 'required|unique:posts',
            'body' => 'required',
        ]);

        $post = Post::create([
            'admin_id' => auth('admin')->user()->id,
            'title' => $req['title'],
            'body' => $req['body'],
        ]);

        Cache::put("post-{$post->id}", $post);
    }

    public function fetchAll()
    {
        // $this->authorize('fetchAll', Post::class);

        $posts = Post::with('admin')->orderBy('id', 'desc')->paginate(10);

        return response()->json($posts);
    }

    public function delete(Request $req)
    {

        Post::find($req['post_id'])->delete();
    }

    public function fetch($postId)
    {
        if (!$post = Cache::get("post-{$postId}")) {
            $post = Post::find($postId);
        }

        return response()->json($post);
    }

    public function update(Request $req)
    {
        $post = Post::find(2);

        //        auth('admin')->user()->can('update', $post);
        $this->authorize('update', $post);


        Post::find($req['id'])->update([
            'title' => $req['title'],
            'body' => $req['body'],
        ]);
    }
}
