<?php

namespace App\Http\Controllers;

use App\Events\CommentStore;
use App\Jobs\ScrapWebsite;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function fetch($postId)
    {
        $post = Post::find($postId);

        return response()->json($post);
    }

    public function scrap()
    {
        $urls = collect([
            'https://blog.faradars.org/category/practical/programming/',
            'https://blog.faradars.org/category/practical/programming/page/2/',
            'https://blog.faradars.org/category/practical/programming/page/3/'
        ]);
        $title = '.card-title';
        $body = '.card-description';

        ScrapWebsite::dispatch($urls, $title, $body);

        //        dd($body);
    }

    public function commentStore(Request $req)
    {
        Comment::create([
            'name' => $req['name'],
            'body' => $req['body'],
            'post_id' => $req['post_id'],
        ]);

        event(new CommentStore);
    }


    public function fetchComment($postId)
    {
        $comments = Comment::where('post_id', $postId)->get();

        return response()->json($comments);
    }


    public function factory()
    {
        $posts = Post::factory()->count(10)->make();

        foreach ($posts as $post) {
            Post::create($post->toArray());
        }
    }
}
