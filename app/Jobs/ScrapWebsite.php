<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class ScrapWebsite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $urls = null;
    public $title = '';
    public $body = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($urls, $title , $body)
    {
        $this->urls = $urls;
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->urls->each(function ($url) {
            $response = Http::post('193.105.234.119:8080/api/scrap', [
                'url' => $url,
                'query' => [
                    'title' => [
                        'navigation' => $this->title,
                    ],
                    'body' => [
                        'navigation' => $this->body,
                    ]
                ],
            ]);
            $body = $response->object();
        });
    }
}
