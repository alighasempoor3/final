<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\PostController as AdminPostController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::get('/users', [AuthController::class, 'usersFetch'])->middleware('auth:sanctum');


Route::get('/auth/check', [AuthController::class, 'authCheck']);
Route::get('/auth/super_admin/check', [AuthController::class, 'superAdminAuthCheck']);
Route::get('/auth/admin/check', [AuthController::class, 'adminAuthCheck']);

Route::post('/admin/register', [AuthController::class, 'adminRegister']);
Route::post('/admin/login', [AuthController::class, 'adminLogin']);


Route::middleware(['auth:admin'])->controller(AdminPostController::class)->prefix('admin')
    ->group(function () {

        Route::post('/post', 'store');
        Route::get('/post', 'fetchAll');
        Route::put('/post', 'update');
        Route::delete('/post', 'delete');
        Route::get('/post/fetch/{postId}', 'fetch');

    });


Route::get('/post/fetch/{postId}', [PostController::class, 'fetch']);
Route::get('/factory/post', [PostController::class, 'factory']);

